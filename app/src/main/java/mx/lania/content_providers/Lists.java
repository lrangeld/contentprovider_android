package mx.lania.content_providers;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import mx.lania.content_providers.customproviders.BookProvider;
import mx.lania.content_providers.customproviders.MemberProvider;
import mx.lania.content_providers.db.models.Book;
import mx.lania.content_providers.db.models.Editor;
import mx.lania.content_providers.db.models.Member;

public class Lists extends AppCompatActivity {

    private ArrayList<String> listDB;
    private Book BookController;
    private Member MemberController;
    private Editor EditorController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_lists);

        switch (getIntent().getIntExtra("type", 5)){
            case MainActivity.SHOW_BOOKS:
                loadBooks();
                break;
            case MainActivity.SHOW_MEMBERS:
                loadMembers();
                break;
            case MainActivity.SHOW_EDITORS:
                loadEditors();
                break;
            default:
        }
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);


        for (String item : listDB) {
            TextView tv = new TextView(this);
            tv.setText(item);
            tv.setLayoutParams(params);
            layout.addView(tv);
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ScrollView sv = new ScrollView(this);
        sv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        sv.setVerticalScrollBarEnabled(true);
        sv.addView(layout, layoutParams);

        this.setContentView(sv);
    }

    private void loadBooks(){
        this.listDB = new ArrayList<>();
        Cursor c = getContentResolver().query(BookProvider.SELECT_CONTENT_URI, null, null, null, null);

        if (c.moveToFirst())
            do {
                String  book = "";
                book += "Id: "+c.getString(3) +"\n";
                book += "Título: "+c.getString(2) +"\n";
                book += "Disponiblidad: "+c.getString(0) +"\n";
                book += "Precio: "+c.getString(1) +"\n";
                book += "Autor: "+c.getString(4) +"\n";

                this.listDB.add(book);
            } while (c.moveToNext());
    }

    private void loadMembers(){
        this.listDB = new ArrayList<>();
        Cursor c = getContentResolver().query(MemberProvider.SELECT_CONTENT_URI, null, null, null, null);

        if (c.moveToFirst())
            do {
                String  Member = "";
                Member += "Id: "+c.getString(3) +"\n";
                Member += "Nombre: "+c.getString(4) +"\n";
                Member += "Fecha nacimiento: "+c.getString(1) +"\n";
                Member += "Dirección: "+c.getString(2) +"\n";
                Member += "Fecha registro: "+c.getString(0) +"\n";

                this.listDB.add(Member);
            } while (c.moveToNext());
    }

    private void loadEditors(){
        this.listDB = new ArrayList<>();
        EditorController = new Editor(Lists.this);
        //Mostrar todos los editores
        EditorController.openConnection();
        Cursor c = EditorController.getEditors();

        if (c.moveToFirst())
            do {
                String  Editor = "";
                Editor += "Id: "+c.getString(0) +"\n";
                Editor += "Nombre: "+c.getString(1) +"\n";
                Editor += "Dirección: "+c.getString(2) +"\n";

                this.listDB.add(Editor);
            } while (c.moveToNext());
        EditorController.closeConnection();
    }
}