package mx.lania.content_providers.db.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import mx.lania.content_providers.db.helpers.DatabaseHelper;

public class Member {

    //Campos de tabla
    public static final String ID_FIELD = "_id";
    public static final String NAME_FIELD = "nombre";
    public static final String BIRTHDATE_FIELD = "fecha_nacimiento";
    public static final String ADDRESS_FIELD = "direccion";
    public static final String REGISTRY_FIELD = "fecha_registro";

    //Informacion de la tabla
    public static final String TABLE_NAME = "miembros";

    //Consulta de eliminacion
    public static final String MEMBER_DROP_TABLE = "DROP TABLE IF EXISTS "+ TABLE_NAME;

    //Creacion de la tabla
    public static final String MEMBER_TABLE_CREATE = "create table "+ TABLE_NAME +" ("
            + ID_FIELD + " integer primary key autoincrement, "
            + NAME_FIELD + " text not null, "
            + BIRTHDATE_FIELD + " text not null, "
            + ADDRESS_FIELD + " text not null, "
            + REGISTRY_FIELD + " text not null);";

    final Context context;
    DatabaseHelper DBHelper;
    public SQLiteDatabase db;

    //Contructor de clase
    public Member(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
        Log.e("LOG", MEMBER_TABLE_CREATE);
    }

    //Abrir conexion
    public Member openConnection() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //Cerrar conexion
    public void closeConnection() {
        DBHelper.close();
    }

    //Insertar un nuevo miembro
    public long insertMember(String name, String birthdate, String address, String registry) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(NAME_FIELD, name);
        initialValues.put(BIRTHDATE_FIELD, birthdate);
        initialValues.put(ADDRESS_FIELD, address);
        initialValues.put(REGISTRY_FIELD, registry);
        return db.insert(TABLE_NAME, null, initialValues);
    }

    //Obtener todos los miembro
    public Cursor getMembers() {
        return db.query(TABLE_NAME, new String[]{ID_FIELD, NAME_FIELD, BIRTHDATE_FIELD, ADDRESS_FIELD, REGISTRY_FIELD},
                null, null, null, null, null);
    }


    //Obtener un miembro
    public Cursor getMember(long _id) throws SQLException {
        Cursor cursor = db.query(true,
                TABLE_NAME,
                new String[]{ID_FIELD, NAME_FIELD, BIRTHDATE_FIELD, ADDRESS_FIELD, REGISTRY_FIELD},
                ID_FIELD + "=" + _id,
                null,
                null,
                null,
                null,
                null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor;
    }


    //Actualizar miembro
    public boolean updateMember(long _id, String name, String birthdate, String address, String registry) {
        ContentValues args = new ContentValues();
        args.put(NAME_FIELD, name);
        args.put(BIRTHDATE_FIELD, birthdate);
        args.put(ADDRESS_FIELD, address);
        args.put(REGISTRY_FIELD, registry);
        return db.update(TABLE_NAME, args, ID_FIELD + "=" + _id, null) > 0;
    }

    //Borrar miembro
    public boolean deleteMember(long _id) {
        return db.delete(TABLE_NAME, ID_FIELD + "=" + _id, null) > 0;
    }
}

