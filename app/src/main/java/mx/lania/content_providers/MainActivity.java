package mx.lania.content_providers;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import mx.lania.content_providers.forms.*;

public class MainActivity extends AppCompatActivity {

    private Button btnAddBook;
    private Button btnAddMember;
    private Button btnAddEditor;
    private Button btnShowBooks;
    private Button btnShowMembers;
    private Button btnShowEditors;
    private Button btnSeekUpdateMember;

    public static final int SHOW_BOOKS = 0;
    public static final int SHOW_MEMBERS = 1;
    public static final int SHOW_EDITORS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Enlazar componentes del view
        btnAddBook = (Button) findViewById(R.id.btnAddBook);
        btnAddMember = (Button) findViewById(R.id.btnAddMember);
        btnAddEditor = (Button) findViewById(R.id.btnAddEditor);
        btnShowBooks = (Button) findViewById(R.id.btnShowBooks);
        btnShowMembers = (Button) findViewById(R.id.btnShowMembers);
        btnShowEditors = (Button) findViewById(R.id.btnShowEditors);
        btnSeekUpdateMember = (Button) findViewById(R.id.btnSeekUpdateMember);

        //Eventos de accion botones
        //Boton agregar Libro
        btnAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, BookForm.class);
                startActivity(i);
            }
        });

        //Boton agregar Miembro
        btnAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MemberForm.class);
                startActivity(i);
            }
        });

        //Boton agregar Editor
        btnAddEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, EditorForm.class);
                startActivity(i);
            }
        });

        //Boton mostrar libros
        btnShowBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Lists.class);
                i.putExtra("type", SHOW_BOOKS);
                startActivity(i);
            }
        });

        //Boton mostrar miembros
        btnShowMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Lists.class);
                i.putExtra("type", SHOW_MEMBERS);
                startActivity(i);
            }
        });

        //Boton mostrar editores
        btnShowEditors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Lists.class);
                i.putExtra("type", SHOW_EDITORS);
                startActivity(i);
            }
        });

        //Boton buscar/actualizar miembros
        btnSeekUpdateMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, EditDeleteMember.class);
                startActivity(i);
            }
        });
    }
}