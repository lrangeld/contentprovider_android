package mx.lania.content_providers.customproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import mx.lania.content_providers.db.models.Book;
import mx.lania.content_providers.db.models.Member;

public class MemberProvider extends ContentProvider {

    private Member MemberController;

    //Datos del provedor
    public static final String PROVIDER_NAME = "mx.lania.content_providers.customproviders.member-provider";
    public static final String URL = "content://" + PROVIDER_NAME + "/members";
    public static final Uri SELECT_CONTENT_URI = Uri.parse(URL);
    public  static final String SELECT_BY_NAME_URL = "content://" + PROVIDER_NAME + "/members-name/";
    public  static final String SELECT_BY_ID_URL = "content://" + PROVIDER_NAME + "/members/";

    //Valores para las uris
    public static final int MEMBERS = 1;
    public static final int MEMBERS_ID = 2;
    public static final int MEMBERS_NAME = 3;

    //Mapa de proyecciones
    private static final Map<String, String> MEMBERS_MAP;
    //Mapeador de valores de uri
    public static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(PROVIDER_NAME, "members", MEMBERS);
        URI_MATCHER.addURI(PROVIDER_NAME, "members-name/*", MEMBERS_NAME);
        URI_MATCHER.addURI(PROVIDER_NAME, "members/#", MEMBERS_ID);
        MEMBERS_MAP = new HashMap<>();
        MEMBERS_MAP.put(Member.ID_FIELD, Member.ID_FIELD);
        MEMBERS_MAP.put(Member.NAME_FIELD, Member.NAME_FIELD);
        MEMBERS_MAP.put(Member.BIRTHDATE_FIELD, Member.BIRTHDATE_FIELD);
        MEMBERS_MAP.put(Member.ADDRESS_FIELD, Member.ADDRESS_FIELD);
        MEMBERS_MAP.put(Member.REGISTRY_FIELD, Member.REGISTRY_FIELD);
    }


    @Override
    public boolean onCreate() {
        MemberController = new Member(getContext());
        return MemberController.openConnection() != null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(Member.TABLE_NAME);

        switch (URI_MATCHER.match(uri)) {
            case MEMBERS:
                queryBuilder.setProjectionMap(MEMBERS_MAP);
                break;
            case MEMBERS_NAME:
                queryBuilder.appendWhere(Member.NAME_FIELD + " = \"" + uri.getLastPathSegment().trim() + "\"");
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if(TextUtils.isEmpty(sortOrder)) {
            sortOrder = Member.NAME_FIELD;
        }
        Cursor cursor = queryBuilder.query(MemberController.db, projection, selection, selectionArgs, null, null, sortOrder);
        //Register to watch a content URI for changes
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long row = MemberController.db.insert(Member.TABLE_NAME, "", values);
        if(row > 0) {
            Uri newUri = ContentUris.withAppendedId(SELECT_CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(newUri, null);
            return newUri;
        }
        throw new SQLiteException("Fail to add a new record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (URI_MATCHER.match(uri)) {
            case MEMBERS:
                break;
            case MEMBERS_ID:
                if (!TextUtils.isEmpty(selection)) {
                    selection = Member.ID_FIELD + " = " + uri.getLastPathSegment() + " AND " + selection;
                } else {
                    selection = Member.ID_FIELD + " = " + uri.getLastPathSegment();
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        count = MemberController.db.delete(Member.TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (URI_MATCHER.match(uri)) {
            case MEMBERS:
                break;
            case MEMBERS_ID:
                if (!TextUtils.isEmpty(selection)) {
                    selection = Member.ID_FIELD + " = " + uri.getLastPathSegment() + " AND " + selection;
                } else {
                    selection = Member.ID_FIELD + " = " + uri.getLastPathSegment();
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        count = MemberController.db.update(Member.TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
