package mx.lania.content_providers.customproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import mx.lania.content_providers.db.models.Book;

public class BookProvider extends ContentProvider {

    private Book BookController;

    //Datos del provedor
    public static final String PROVIDER_NAME = "mx.lania.content_providers.customproviders.book-provider";
    public static final String URL = "content://" + PROVIDER_NAME + "/books";
    public static final Uri SELECT_CONTENT_URI = Uri.parse(URL);

    //Valores para las uris
    public static final int BOOKS = 1;
    public static final int BOOK_ID = 2;

    //Mapa de proyecciones
    private static final Map<String, String> BOOKS_MAP;
    //Mapeador de valores de uri
    public static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(PROVIDER_NAME, "books", BOOKS);
        URI_MATCHER.addURI(PROVIDER_NAME, "books/#", BOOK_ID);
        BOOKS_MAP = new HashMap<>();
        BOOKS_MAP.put(Book.ID_FIELD, Book.ID_FIELD);
        BOOKS_MAP.put(Book.TITLE_FIELD, Book.TITLE_FIELD);
        BOOKS_MAP.put(Book.AVAILABILITY_FIELD, Book.AVAILABILITY_FIELD);
        BOOKS_MAP.put(Book.PRICE_FIELD, Book.PRICE_FIELD);
        BOOKS_MAP.put(Book.AUTHOR_FIELD, Book.AUTHOR_FIELD);
    }

    @Override
    public boolean onCreate() {
        //Controlador del libro
        BookController = new Book(getContext());
        return BookController.openConnection() != null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(Book.TABLE_NAME);

        switch (URI_MATCHER.match(uri)) {
            case BOOKS:
                queryBuilder.setProjectionMap(BOOKS_MAP);
                break;
            case BOOK_ID:
                queryBuilder.appendWhere(Book.ID_FIELD + " = " + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if(TextUtils.isEmpty(sortOrder)) {
            sortOrder = Book.TITLE_FIELD;
        }
        Cursor cursor = queryBuilder.query(BookController.db, projection, selection, selectionArgs, null, null, sortOrder);
        //Register to watch a content URI for changes
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long row = BookController.db.insert(Book.TABLE_NAME, "", values);
        if(row > 0) {
            Uri newUri = ContentUris.withAppendedId(SELECT_CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(newUri, null);
            return newUri;
        }
        throw new SQLiteException("Fail to add a new record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
